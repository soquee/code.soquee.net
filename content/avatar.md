+++
title    = "avatar"
date     = "2019-05-26T12:08:30-05:00"
repo     = "https://codeberg.org/soquee/avatar"
todo     = "https://codeberg.org/soquee/avatar/issues"
category = "Modules"
+++

Package **`avatar`** contains functions for creating default user avatars.
