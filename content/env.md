+++
title = "env"
date  = "2019-05-26T12:08:30-05:00"
repo  = "https://codeberg.org/soquee/env"
todo  = "https://codeberg.org/soquee/env/issues"
category = "Modules"
+++

Package **`env`** can be used to load environment variables from files.
