+++
title = "login"
date  = "2023-11-24T21:33:42-05:00"
repo  = "https://codeberg.org/soquee/login"
todo  = "https://codeberg.org/soquee/login/issues"
category = "Modules"
+++

Package **`login`** handles password hashing and login cookies.
