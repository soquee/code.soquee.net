+++
title = "migration"
date  = "2019-05-26T12:08:30-05:00"
repo  = "https://codeberg.org/soquee/migration"
todo  = "https://codeberg.org/soquee/migration/issues"
category = "Modules"
+++

Package **`migration`** contains functions for generating and finding PostgreSQL
database migrations using [`pgx`](https://pkg.go.dev/github.com/jackc/pgx).
