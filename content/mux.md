+++
title = "mux"
date  = "2019-05-26T12:08:30-05:00"
repo  = "https://codeberg.org/soquee/mux"
todo  = "https://codeberg.org/soquee/mux/issues"
category = "Modules"
+++

Package **`mux`** is a Go HTTP multiplexer that provided typed route parameters.
