+++
title    = "norm"
date     = "2023-12-07T10:15:26-05:00"
repo  = "https://codeberg.org/soquee/norm"
todo  = "https://codeberg.org/soquee/norm/issues"
category = "Modules"
+++

Package **`norm`** contains middleware for normalizing HTTP forms using PRECIS.
