+++
title = "otp"
date  = "2019-05-26T13:22:05-05:00"
repo  = "https://codeberg.org/soquee/otp"
todo  = "https://codeberg.org/soquee/otp/issues"
category = "Modules"
+++

Package **`otp`** implements HOTP and TOTP one-time passwords.
