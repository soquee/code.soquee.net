+++
title    = "paddle"
date     = "2020-04-02T09:57:11-04:00"
repo     = "https://codeberg.org/soquee/paddle"
category = "Unmaintained"
summary  = "A Go SDK for the Paddle HTTP API."
nodoc    = true
+++

Package **`paddle`** is an ***unmaintained*** Go SDK for the Paddle payment
service's HTTP API.
