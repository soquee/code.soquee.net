+++
title    = "coupon"
date     = "2020-04-02T10:00:00-04:00"
repo     = "https://codeberg.org/soquee/paddle"
category = ""
+++

Package coupon provides APIs related to coupons.
