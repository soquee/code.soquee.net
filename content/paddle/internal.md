+++
title    = "internal"
date     = "2020-04-02T10:00:40-04:00"
repo     = "https://codeberg.org/soquee/paddle"
category = ""
+++

Package internal contains unexported functionality required by all paddle
packages.
