+++
title    = "license"
date     = "2020-04-02T10:00:54-04:00"
repo     = "https://codeberg.org/soquee/paddle"
category = ""
+++

Package license provides APIs related to licenses.
