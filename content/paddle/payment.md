+++
title    = "payment"
date     = "2020-04-02T10:00:57-04:00"
repo     = "https://codeberg.org/soquee/paddle"
category = ""
+++

Package payment provides APIs related to payments.
