+++
title    = "product"
date     = "2020-04-02T10:01:05-04:00"
repo     = "https://codeberg.org/soquee/paddle"
category = ""
+++

Package product provides APIs related to products.
