+++
title    = "pkgzip"
date     = "2019-05-26T12:08:30-05:00"
repo     = "https://codeberg.org/soquee/pkgzip"
summary  = "The pkgzip command bundles assets into a Go package."
category = "Unmaintained"
nodocs   = true
+++

The **`pkgzip`** command bundles assets into a Go package.

---

Now that Go 1.16 includes embedding resources, this package is **unmaintained**.
For more information see the [`embed`](https://pkg.go.dev/embed) package.

---

For more information install the command and run `pkgzip -help`, or run `go run
code.soquee.net/pkgzip -help`.

To use this tool in your Go project, add it to your `tools.go` file, and add a
[Go Generate] line (this doesn't have to be in `tools.go`) similar to the one in
the following example:

```go
//go:generate go run code.soquee.net/pkgzip -m -f -src assets -pkg assetsfs

// +build tools

package main

import (
	_ "code.soquee.net/pkgzip"
)
```

Then update your `go.mod` file with the specific version you want (use `go get
-u` to get the latest), and run `go generate tools.go` (or wherever you put the
comment) to generate your assets package.
It's probably easiest to do this from your Makefile like so:

```make
ASSETS!=find myassetstree/

mybin: internal/assetsfs/pkgzip.go
	go build -o $@

internal/assetsfs/pkgzip.go: $(ASSETS)
	go generate tools.go
```

[Go Generate]: https://golang.org/pkg/cmd/go/#hdr-Generate_Go_files_by_processing_source
