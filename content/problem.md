+++
title = "problem"
date  = "2019-05-26T12:08:30-05:00"
repo  = "https://codeberg.org/soquee/problem"
todo  = "https://codeberg.org/soquee/problem/issues"
category = "Modules"
+++

Package **`problem`** implements errors similar to the ones described by
[RFC7807].

[RFC7807]: https://tools.ietf.org/html/rfc7807
