+++
title = "query"
date  = "2019-05-26T12:08:30-05:00"
repo  = "https://codeberg.org/soquee/query"
todo  = "https://codeberg.org/soquee/query/issues"
category = "Modules"
+++

Package **`query`** parses the simple query language used for full text search
in the Soquee issue tracker.
