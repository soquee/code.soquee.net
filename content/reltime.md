+++
title = "reltime"
date  = "2019-05-26T12:08:30-05:00"
repo  = "https://codeberg.org/soquee/reltime"
todo  = "https://codeberg.org/soquee/reltime/issues"
category = "Modules"
+++

Package **`reltime`** implements a "time ago" algorithm.
