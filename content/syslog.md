+++
title    = "syslog"
date     = "2022-11-06T06:57:29-05:00"
repo     = "https://codeberg.org/soquee/syslog"
todo     = "https://codeberg.org/soquee/syslog/issues"
category = "Modules"
+++

Package **`syslog`** is a fork of the `syslog` package from the standard library
that allows writing logs to locations other than the network or the local syslog
daemon.
