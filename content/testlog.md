+++
title = "testlog"
date  = "2019-05-26T12:08:30-05:00"
repo  = "https://codeberg.org/soquee/testlog"
todo  = "https://codeberg.org/soquee/testlog/issues"
summary = "Package testlog is a log.Logger that proxies to the Log function on a testing.T."
category = "Modules"
+++

Package **`testlog`** is a `log.Logger` that proxies to the `Log` function on a
`testing.T`.

It is used to group log messages under the tests that generated them in test
output and to only show those messages if the test that generated them failed.
