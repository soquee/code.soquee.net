+++
title = "tmpl"
date  = "2019-05-26T13:22:05-05:00"
repo  = "https://codeberg.org/soquee/tmpl"
todo  = "https://codeberg.org/soquee/tmpl/issues"
category = "Modules"
+++

Package **`tmpl`** handles loading and rendering HTML templates.
