+++
title    = "versions"
date     = "2020-03-22T09:12:15-04:00"
repo     = "https://codeberg.org/soquee/versions"
category = "Unmaintained"
nodocs   = true
+++

Soquee **Versions** is a tool for managing Git repos written in Rust and GTK.
